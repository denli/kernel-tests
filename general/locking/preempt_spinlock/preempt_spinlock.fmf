summary: Preempt spinlock test
description: |
    In PREEMPT_RT, spin_lock() are coverted on rt_mutex, and could be preempted.
    The test simply verify the behavior.

    The writer thread (SCHED_FIFO:1) would preempt the locker thread (SCHED_NORMAL)
    in the test, and they both bind to cpu0.

    PARAMETERS

    Test Inputs:
        Execute 'make' to compile preempt_spinlock.ko and expect a return of 0.
        Execute 'insmod' to install the module preempt_spinlock.ko and expect a return of 0.
        Execute 'grep PASS' against the test_dmesg.txt file and expect a return of 0.

    Expected result:
        :: [   PASS   ] :: compile the preempt_spinlock.ko (Expected 0, got 0)
        :: [   PASS   ] :: Command 'insmod preempt_spinlock/preempt_spinlock.ko' (Expected 0, got 0)
        :: [   PASS   ] :: The module should report PASS in dmesg (Expected 0, got 0)

    Results location:
        output.txt | taskout.log, log is dependent upon the test executor.
contact: Chunyu Hu <chuhu@redhat.com>
test: bash runtest.sh
framework: beakerlib
duration: 45m
require:
    - beakerlib
    - gcc
    - libgcc
    - elfutils-libelf-devel
    - openssl-devel
    - type: file
      pattern:
          - /general/include
          - /kernel-include
recommend:
    - git
    - glibc
    - bison
    - flex
id: 43375efc-a271-4da3-8136-7aa4a1edf036
extra-summary: kernel-tests/general/locking/preempt_spinlock
extra-task: kernel-tests/general/locking/preempt_spinlock
