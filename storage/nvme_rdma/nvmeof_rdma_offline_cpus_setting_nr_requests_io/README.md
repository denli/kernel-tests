# storage/nvme_rdma/nvmeof_rdma_offline_cpus_setting_nr_requests_io

Storage: nvmeof rdma offline cpus setting nr_requests during io

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
